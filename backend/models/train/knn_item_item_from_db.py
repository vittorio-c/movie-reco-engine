import pickle
import petl as etl

from surprise import Reader
from surprise import Dataset
from surprise import KNNBasic
from surprise.dataset import DatasetAutoFolds

from app.queries.ratings import get_ratings

ratings = get_ratings()

table = etl.fromdicts(ratings)
df_rating = etl.todataframe(table)

print(f'Shape of loaded DF is {df_rating.shape}')

reader = Reader(rating_scale=(0.5, 5))

data: DatasetAutoFolds = Dataset.load_from_df(df_rating[['user_id', 'ml_id', 'rating']], reader)
trainset = data.build_full_trainset()

sim_options = {'name': 'cosine',
               'user_based': False  # compute  similarities between items
               }

knn = KNNBasic(sim_options=sim_options)
knn.fit(trainset)

pickle.dump(knn, open('models/serialized/knn_item_item__BIG__NEW.pkl', 'wb'))

print('DONE training the model')
