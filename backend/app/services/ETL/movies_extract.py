from app.services.ETL.csv_extract_params import CsvExtractParams
from app.services.ETL.movies_extractor import MoviesBatchExtractor

params = CsvExtractParams()
params.csv_path = "/home/<user_name>/Documents/movie_lens_data_big/movies_enrichment.csv"
# This will import batch_count * batch_size quantity of movies
params.start_after = 0
params.batch_count = 10
params.batch_size = 100
params.topic = "ml2ml_movies"

movies_extractor = MoviesBatchExtractor(params)

movies_extractor.extract()
