module.exports = {
  content: ["./public/**/*.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        teal_cust_1: "#ebf7f5",
        dark_green_cust_2: "#031411",
        dark_green_cust_1: "#0d5446",
        blue_cust_2: "#1676a0",
        red_cust_1: "#a01631",
        blue_green_cust_1: "#16a085",
        bg_semi_75: "rgba(0, 0, 0, 0.75)",
      },
    },
  },
  plugins: [],
};
