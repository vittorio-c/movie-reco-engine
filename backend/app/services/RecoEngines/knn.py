import os
import pickle

from surprise import KNNBasic, Trainset


class KnnRecommender:
    def __init__(self, ml_id, model):
        self.ml_id = ml_id
        self.model = model
        self.k_neighbors: int = 5

    def recommend(self):
        trained_set: Trainset = self.model.trainset

        inner_id = trained_set.to_inner_iid(self.ml_id)
        neighbors_as_inner_ids = self.model.get_neighbors(inner_id, self.k_neighbors)

        return [trained_set.to_raw_iid(inner_id) for inner_id in neighbors_as_inner_ids]

    @staticmethod
    def get_model():
        cwd = os.getcwd()

        knn: KNNBasic = pickle.load(
            open(f"{cwd}/models/serialized/knn_item_item.pkl", "rb")
        )
        return knn
