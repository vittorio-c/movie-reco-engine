class Movie:
    def __init__(self):
        self._ml_id: int = 0
        self._title: str = ""
        self._release_year: int = 0
        self._genres: list = []
        self._tags: list = []
        self._budget: float = 0.0
        self._revenue: float = 0.0
        self._profit: float = 0.0
        self._companies: list = []
        self._companies_iso: list = []
        self._poster_url: str = ""
        self._ml_mean_rating: float = 0.0
        self._tmdb_id: int = 0

    def to_json(self):
        return {
            "ml_id": self.ml_id,
            "title": self.title,
            "release_year": self.release_year,
            "genres": self.genres,
            "tags": self.tags,
            "budget": self.budget,
            "revenue": self.revenue,
            "profit": self.profit,
            "companies": self.companies,
            "companies_iso": self.companies_iso,
            "poster_url": self.poster_url,
            "ml_mean_rating": self.ml_mean_rating,
        }

    @property
    def ml_id(self):
        return self._ml_id

    @ml_id.setter
    def ml_id(self, value):
        self._ml_id = value

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def release_year(self):
        return self._release_year

    @release_year.setter
    def release_year(self, value):
        self._release_year = value

    @property
    def genres(self):
        return self._genres

    @genres.setter
    def genres(self, value):
        self._genres = value

    @property
    def tags(self):
        return self._tags

    @tags.setter
    def tags(self, value):
        self._tags = value

    @property
    def budget(self):
        return self._budget

    @budget.setter
    def budget(self, value):
        self._budget = value

    @property
    def revenue(self):
        return self._revenue

    @revenue.setter
    def revenue(self, value):
        self._revenue = value

    @property
    def profit(self):
        return self._profit

    @profit.setter
    def profit(self, value):
        self._profit = value

    @property
    def companies(self):
        return self._companies

    @companies.setter
    def companies(self, value):
        self._companies = value

    @property
    def companies_iso(self):
        return self._companies_iso

    @companies_iso.setter
    def companies_iso(self, value):
        self._companies_iso = value

    @property
    def poster_url(self):
        return self._poster_url

    @poster_url.setter
    def poster_url(self, value):
        self._poster_url = value

    @property
    def ml_mean_rating(self):
        return self._ml_mean_rating

    @ml_mean_rating.setter
    def ml_mean_rating(self, value):
        if value is not None:
            self._ml_mean_rating = str(round(value, 2))
        else:
            self._ml_mean_rating = None

    @property
    def tmdb_id(self):
        return self._tmdb_id

    @tmdb_id.setter
    def tmdb_id(self, value):
        self._tmdb_id = value


class PredictedMovie(Movie):
    def __init__(self):
        super().__init__()
        self._prediction: int = 0

    def to_json(self):
        base = super().to_json()
        base['prediction'] = self.prediction

        return base

    @property
    def prediction(self):
        return self._prediction

    @prediction.setter
    def prediction(self, value):
        self._prediction = str(round(value, 2))


class MovieFactory:
    def __init__(self, payload: dict):
        self.payload = payload

    def factory(self):
        movie = Movie()
        try:
            movie.title = self.payload["title"]
            movie.ml_id = self.payload["ml_id"]
            movie.release_year = self.payload["release_year"]
        except KeyError as e:
            print(e)
            return None

        movie.budget = self.payload.get("budget")
        movie.revenue = self.payload.get("revenue")
        movie.profit = self.payload.get("profit")

        movie.genres = self.payload.get("genres")
        movie.tags = self.payload.get("tags")
        movie.companies = self.payload.get("companies")
        movie.companies_iso = self.payload.get("companies_iso")
        movie.poster_url = self.payload.get("tmdb_poster_path")
        movie.ml_mean_rating = self.payload.get("ml_mean_rating")

        movie.tmdb_id = self.payload.get("tmdb_id")

        return movie


class MovieFactoryWithPrediction(MovieFactory):
    def __init__(self, payload):
        super().__init__(payload)

    def factory(self):
        movie = PredictedMovie()
        try:
            movie.title = self.payload["title"]
            movie.ml_id = self.payload["ml_id"]
            movie.release_year = self.payload["release_year"]
            movie.prediction = self.payload["prediction"]
        except KeyError as e:
            print(e)
            return None

        movie.budget = self.payload.get("budget")
        movie.revenue = self.payload.get("revenue")
        movie.profit = self.payload.get("profit")

        movie.genres = self.payload.get("genres")
        movie.tags = self.payload.get("tags")
        movie.companies = self.payload.get("companies")
        movie.companies_iso = self.payload.get("companies_iso")
        movie.poster_url = self.payload.get("tmdb_poster_path")
        movie.ml_mean_rating = self.payload.get("ml_mean_rating")

        movie.tmdb_id = self.payload.get("tmdb_id")

        return movie

