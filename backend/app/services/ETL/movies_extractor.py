import csv
import time
from itertools import islice

from app.services.ETL.csv_extract_params import CsvExtractParams
from app.services.Kafka.producer_factory import get_producer


class MoviesBatchExtractor:
    def __init__(self, params: CsvExtractParams):
        self.csv_path = params.csv_path
        self.start_after = params.start_after
        self.batch_count = params.batch_count
        self.batch_size = params.batch_size
        self.topic = params.topic
        self.producer = get_producer("localhost:9092")

    def extract(self):
        with open(self.csv_path, "r") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=",")

            batch = []
            batch_size = 0
            batch_count = 0

            for row in islice(reader, self.start_after, None):
                if batch_size >= self.batch_size:
                    print(
                        f"Publishing message n° {batch_count} on topic : {self.topic}\n"
                    )
                    self.producer.send(self.topic, batch)
                    batch = []
                    batch_size = 0
                    batch_count += 1
                    time.sleep(1)

                batch.append(row)
                batch_size += 1

                if batch_count >= self.batch_count:
                    break

            if batch:
                self.producer.send(self.topic, batch)
