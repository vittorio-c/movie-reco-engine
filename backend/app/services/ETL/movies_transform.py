import petl as etl
from flask import current_app

from app.services.ETL.transform_utilities import *
from app.services.Kafka.consumer_factory import get_consumer
from kafka.consumer.fetcher import ConsumerRecord
from pymongo.collection import Collection


class MoviesTransformer:
    def __init__(self, payload):
        self.movies_payload = payload
        self.movies_out = None

    def order(self):
        self.movies_out = (
            self.movies_out.movefield("title", 1)
                .movefield("release_year", 2)
                .movefield("genres", 3)
                .movefield("tags", 4)
                .movefield("budget", 5)
                .movefield("revenue", 6)
                .movefield("profit", 7)
                .movefield("companies", 8)
                .movefield("companies_iso", 9)
                .movefield("tmdb_vote_count", 10)
                .movefield("tmdb_vote_avg", 11)
                .movefield("tmdb_poster_path", 12)
                .movefield("ml_id", 13)
                .movefield("tmdb_id", 14)
                .movefield("imdb_id", 15)
        )

        return self

    def join_tags(self):
        movie_ids = list(etl.values(self.movies_out, "ml_id"))
        tags = (
            etl.fromcsv("/home/<user_name>/Documents/movie_lens_data_big/tags.csv")
                .convert("movieId", int)
                .selectin("movieId", movie_ids)
                .aggregate("movieId", list, "tag")
        )

        self.movies_out = etl.leftjoin(
            self.movies_out, tags, rkey="movieId", lkey="ml_id"
        ).rename({"value": "tags"})

        return self

    def join_links(self):
        movie_ids = list(etl.values(self.movies_out, "ml_id"))
        links = (
            etl.fromcsv("/home/<user_name>/Documents/movie_lens_data_big/links.csv")
                .convert("movieId", int)
                .selectin("movieId", movie_ids)
        )

        self.movies_out = (
            etl.join(self.movies_out, links, lkey="ml_id", rkey="movieId")
                .rename({"imdbId": "imdb_id", "tmdbId": "tmdb_id"})
                .convert("imdb_id", int)
                .convert("tmdb_id", int)
        )

        return self

    def transform_movies(self):
        self.movies_out = (
            etl.fromdicts(self.movies_payload)
                .convert("tmdb_budget", float)
                .convert("tmdb_revenue", float)
                .convert("tmdb_vote_avg", float)
                .convert("tmdb_vote_count", float)
                .addfield("profit", calculate_profit)
                .addfield("genre_list", split_genres)
                .addfield("companies", split_companies)
                .addfield("release_year", extract_release_year)
                .addfield("companies_iso", split_companies_iso)
                .convert("release_year", int)
                .cutout("genres")
                .cutout("tmdb_companie_name")
                .cutout("tmdb_companie_iso")
                .convert("movieId", int)
                .rename(
                {
                    "genre_list": "genres",
                    "movieId": "ml_id",
                    "tmdb_budget": "budget",
                    "tmdb_revenue": "revenue",
                }
            )
                .sub("title", "\s\((\d.+?)\)", "")
        )

        return self

    def transform(self):
        try:
            self.transform_movies().join_tags().join_links().order()
            return list(etl.dicts(self.movies_out))
        except Exception as e:
            print(e)
            import traceback

            from app.services.Kafka.producer_factory import get_producer

            error = traceback.format_exc()
            producer = get_producer("localhost:9092")
            payload = {"error": error, "movies": self.movies_payload}

            producer.send("ml2ml_movies_error", payload)


consumer = get_consumer("ml2ml_movies", "localhost:9092")
print(consumer)
message: ConsumerRecord
for message in consumer:
    print(f"Consumming message... {message.offset}\n")
    transformer = MoviesTransformer(message.value)
    movies = transformer.transform()

    collection: Collection = current_app.db.movies
    inserted = collection.insert_many(movies)
