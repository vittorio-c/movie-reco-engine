import os
from datetime import timedelta

SECRET_KEY = os.urandom(32)

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Jwt config
JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)

# Cors
BACKEND_CORS_ORIGINS = os.getenv("BACKEND_CORS_ORIGINS").split(",")

# Cache
CACHE_TYPE = "FileSystemCache"
# 2 hours of cache by default
CACHE_DEFAULT_TIMEOUT = 7200
CACHE_DIR = "/tmp/flask-cache"
