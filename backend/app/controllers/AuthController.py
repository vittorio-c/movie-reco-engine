from datetime import timedelta

from flask import jsonify, request, abort
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, get_jwt

from app.bootstrap.jwt import jwt_blacklist
from app.queries.users import find_user


def login():
    user_id = request.json.get("user_id", None)
    user = find_user(user_id)

    if user is None:
        abort(jsonify(message="Bad user id, try again bitch"), 403)

    access_token = create_access_token(identity=user['user_id'])
    # TODO send more info about the user here (name, email) to put in js store
    return jsonify(access_token=access_token, user_id=user['user_id']), 200


@jwt_required(verify_type=False)
def logout():
    token = get_jwt()
    jti = token["jti"]
    ttype = token["type"]
    jwt_blacklist.add(jti)

    return jsonify(msg=f"{ttype.capitalize()} token successfully revoked")


@jwt_required()
def refresh():
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200

