from app.bootstrap.AppBootstrapper import AppBootstrapper
from app.services.ETL.commands import etl_commands_bp
from app.services.Ratings.commands import ratings_commands_bp

app = AppBootstrapper.boostrap()

with app.app_context():
    from app.routes.ratings_bp import ratings_bp
    from app.routes.auth_bp import auth_bp
    from app.routes.movies_bp import movies_bp
    from app.routes.users_bp import users_bp

    # Register routes
    app.register_blueprint(auth_bp, url_prefix='/api/auth')
    app.register_blueprint(movies_bp, url_prefix='/api/movies')
    app.register_blueprint(users_bp, url_prefix='/api/users')
    app.register_blueprint(ratings_bp, url_prefix='/api/ratings')

    # Register commands
    app.register_blueprint(etl_commands_bp)
    app.register_blueprint(ratings_commands_bp)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=80)
