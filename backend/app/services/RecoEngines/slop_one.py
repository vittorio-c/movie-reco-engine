import os
import pickle

from surprise import SlopeOne

from app.main import app
from app.queries.movies import get_movies
from app.queries.ratings import get_ratings_for_user


class SlopOneRecommender:
    def __init__(self, user_id, model):
        self.user_id = user_id
        self.model: SlopeOne = model

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, self.user_id)

    @app.cache.memoize()
    def recommend(self):
        all_movies_ids = [movie['ml_id'] for movie in get_movies()]

        previously_rated_movies = [rating['ml_id'] for rating in get_ratings_for_user(self.user_id)]

        to_predict_movies_ids = [id for id in all_movies_ids if id not in previously_rated_movies]

        predictions = self.all_for_one_user(self.user_id, to_predict_movies_ids)

        # iid is here a 'raw' inner id
        predictions = [(iid, est) for (_, iid, _, est, _) in predictions]

        return sorted(predictions, key=lambda p: p[1], reverse=True)

    def all_for_one_user(self, uid, item_ids):
        predictions = [self.model.predict(uid, iid) for iid in item_ids]

        return predictions

    @staticmethod
    def get_model():
        cwd = os.getcwd()
        slop_one: SlopeOne = pickle.load(open(f"{cwd}/models/serialized/slop_one_regular.pkl", "rb"))

        return slop_one
