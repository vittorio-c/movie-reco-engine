from flask import Blueprint

from app.controllers.RatingController import rate_movies

ratings_bp = Blueprint('ratings_bp', __name__)

ratings_bp.route('/rate/<int:movie_id>/<float:rating>', methods=['POST'])(rate_movies)
