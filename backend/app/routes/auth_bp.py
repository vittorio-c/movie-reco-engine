from flask import Blueprint

from app.controllers.AuthController import login, logout, refresh

auth_bp = Blueprint('auth_bp', __name__)

auth_bp.route('/login', methods=['POST'])(login)
auth_bp.route('/logout', methods=['DELETE'])(logout)
auth_bp.route('/refresh', methods=['GET'])(refresh)
