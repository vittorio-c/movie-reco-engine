from json import JSONEncoder


def set_custom_json_encoder():
    def _default(self, obj):
        return getattr(obj.__class__, "to_json", _default.default)(obj)

    _default.default = JSONEncoder().default
    JSONEncoder.default = _default
