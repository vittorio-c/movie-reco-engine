import click
import pandas as pd
from pandas import DataFrame
from pymongo.collection import Collection

from flask import Blueprint, current_app

etl_commands_bp = Blueprint('etl', __name__)


@etl_commands_bp.cli.command('insert_users')
@click.argument('filepath')
def run(filepath):
    """ Insert users from a .csv file """

    def insert(user_ids: list):
        client = current_app.client
        db = client.ml2ml
        users_collection: Collection = db.users

        user_ids_as_dict = [{'user_id': int(id)} for id in user_ids]
        inserted = users_collection.insert_many(user_ids_as_dict)

        return inserted

    df_rating: DataFrame = pd.read_csv(filepath). \
        sort_values(['movieId']).drop('timestamp', axis=1)

    # Here we are fetching only the users that gave more than 500 ratings
    # among the first 1 000 movies. It returns ~ 25 users only. This subset
    # is very qualified and is supposed to give good results in recommendations.
    df_rating = df_rating[df_rating['movieId'].between(1, 1000)]
    df_rating = df_rating.groupby(['userId']).count()
    df_rating = df_rating[df_rating['rating'] > 500]

    user_ids: list = df_rating.index.tolist()

    insert(user_ids)

    print(f'Command finished ! Inserted ids are : {",".join(str(e) for e in user_ids)}')

