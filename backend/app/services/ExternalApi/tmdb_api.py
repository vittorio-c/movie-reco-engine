import os

from dotenv import dotenv_values
import requests


class TmdbApi:
    def __init__(self):
        cwd = os.getcwd()

        config = dotenv_values(f"{cwd}/app/.env")

        try:
            # Local value... (from .env file)
            api_token = config["TMDB_API_TOKEN"]
        except KeyError:
            # Env value...
            api_token = os.environ["TMDB_API_TOKEN"]

        self.headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {api_token}"
        }
        self.base_url = "https://api.themoviedb.org/3"

    def translations(self, tmdb_id):
        try:
            uri = f"/movie/{tmdb_id}/translations"
            url = f'{self.base_url}/{uri}'
            r = requests.get(url, headers=self.headers)
            r.raise_for_status()

            return r.json()
        except Exception as e:
            print(e)
            return None

    def translated_overview(self, tmdb_id):
        # We will return only French language for now

        try:
            translations = self.translations(tmdb_id)
            fr_overview = [translation["data"]["overview"] for translation in translations['translations']
                           if translation["iso_3166_1"] == "FR"]

            return fr_overview[0]
        except Exception as e:
            print(e)
            return None
