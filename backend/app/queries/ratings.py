import datetime

from pymongo.results import UpdateResult

from flask import current_app

rating_collection = current_app.db.ratings


def get_ratings():
    cursor = rating_collection.find()

    return [rating for rating in cursor]


def get_ratings_for_user(user_id: int):
    cursor = rating_collection.find({'user_id': user_id})

    return [rating for rating in cursor]


def rate_movie(user_id: int, movie_ml_id: int, rating: float) -> UpdateResult:
    find = {'user_id': user_id, 'ml_id': movie_ml_id}
    rated_at = datetime.datetime.now()
    replacement = {'user_id': user_id, 'ml_id': movie_ml_id, 'rating': rating, 'rated_at': rated_at}

    return rating_collection.replace_one(find, replacement, upsert=True)
