import os

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import mean

from flask import Blueprint

ratings_commands_bp = Blueprint('ratings', __name__)


@ratings_commands_bp.cli.command('calculate_mean')
def run():
    """ Calculate mean ratings of movies """

    def     init_spark():
        password = os.environ["MONGODB_PASSWORD"]
        user = os.environ["MONGODB_USER"]
        host = os.environ["MONGODB_HOST"]
        db_auth = os.environ["MONGODB_DB_AUTH"]
        mongo_conn = f"mongodb://{user}:{password}@{host}:27017/{db_auth}"

        conf = SparkConf()

        # Download mongo-spark-connector and its dependencies.
        # This will download all the necessary jars, no need to manually download them :
        conf.set("spark.jars.packages",
                 "org.mongodb.spark:mongo-spark-connector:10.0.1")
        # Give more memory to Spark process, to avoid "java.lang.OutofMemoryError: Java heap space" error :
        conf.set("spark.driver.memory", "2g")

        # Set up read connection :
        conf.set("spark.mongodb.read.connection.uri", mongo_conn)
        conf.set("spark.mongodb.read.database", "ml2ml")
        conf.set("spark.mongodb.read.collection", "ratings")

        # Set up write connection
        conf.set("spark.mongodb.write.connection.uri", mongo_conn)
        conf.set("spark.mongodb.write.database", "ml2ml")
        conf.set("spark.mongodb.write.collection", "movies")
        conf.set("spark.mongodb.write.operationType", "update")
        conf.set("spark.mongodb.write.idFieldList", "ml_id")

        SparkContext(conf=conf)

        return SparkSession \
            .builder \
            .appName('ml2ml') \
            .getOrCreate()

    spark = init_spark()

    # Read ratings
    df = spark.read.format("mongodb").load()

    # Calculate means
    df_grouped = df.groupBy("ml_id").agg(mean("rating").alias("ml_mean_rating")) \
        .sort("ml_id")

    # Insert them in movies
    df_grouped.write.format("mongodb").mode("append").save()
