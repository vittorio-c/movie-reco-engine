class CsvExtractParams:
    _csv_path: str
    _topic: str
    _start_after: int
    _batch_size: int
    _batch_count: int

    @property
    def csv_path(self):
        return self._csv_path

    @csv_path.setter
    def csv_path(self, value):
        self._csv_path = value

    @property
    def topic(self):
        return self._topic

    @topic.setter
    def topic(self, value):
        self._topic = value

    @property
    def start_after(self):
        return self._start_after

    @start_after.setter
    def start_after(self, value):
        self._start_after = value

    @property
    def batch_size(self):
        return self._batch_size

    @batch_size.setter
    def batch_size(self, value):
        self._batch_size = value

    @property
    def batch_count(self):
        return self._batch_count

    @batch_count.setter
    def batch_count(self, value):
        self._batch_count = value
