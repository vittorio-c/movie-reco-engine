import numpy as np
from flask import jsonify, request
import app.queries.movies as query_movie
from app.models.movie import MovieFactory
from app.services.ExternalApi.tmdb_api import TmdbApi
from app.services.RecoEngines.knn import KnnRecommender
from app.utilities.paginate import get_pagination_routes


knn = KnnRecommender.get_model()


def api_movie(movie_id):
    movie = query_movie.get_movie(movie_id)
    return jsonify({"_data": MovieFactory(movie).factory()})


def api_movies():
    # TODO Reduce payload
    page_num = request.args.get("page") or 1
    sorts = request.args.getlist("sorts[]")
    order = request.args.get("order") or -1
    links = get_pagination_routes(page_num, request)
    output = query_movie.get_movies_paginated(15, int(page_num), sorts, order)
    output["_embed"] = links

    return jsonify(output)


# TODO Ajout de prints pour la démo mémoire
def similar_movies(movie_ml_id):
    service = KnnRecommender(movie_ml_id, knn)
    similar_movies_ids: np.ndarray = service.recommend()
    similar = [query_movie.get_movie(int(movie_id)) for movie_id in similar_movies_ids]
    movies = [MovieFactory(movie).factory() for movie in similar]

    return jsonify({"_data": movies})


def api_movie_translations(movie_id):
    service = TmdbApi()
    movie = query_movie.get_movie(movie_id)
    tmdb_id = MovieFactory(movie).factory().tmdb_id

    translations = service.translations(tmdb_id)

    return jsonify({"_data": translations})


def api_movie_overview(movie_id):
    service = TmdbApi()
    movie = query_movie.get_movie(movie_id)
    tmdb_id = MovieFactory(movie).factory().tmdb_id

    translated_overview = service.translated_overview(tmdb_id)

    return jsonify({"_data": translated_overview})


