#! /usr/bin/env bash

apt update && apt install -y git

apt install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    dbus-user-session

mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update

apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose

groupadd docker

usermod -aG docker debian

systemctl restart docker

