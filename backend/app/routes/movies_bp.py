from flask import Blueprint

from app.controllers.MovieController import \
    api_movie, api_movies, similar_movies, \
    api_movie_translations, api_movie_overview

movies_bp = Blueprint('movies_bp', __name__)

movies_bp.route('/<int:movie_id>', methods=['GET'])(api_movie)
movies_bp.route('/', methods=['GET'])(api_movies)
movies_bp.route('/<int:movie_ml_id>/similar-movies', methods=['GET'])(similar_movies)
movies_bp.route('/<int:movie_id>/translations', methods=['GET'])(api_movie_translations)
movies_bp.route('/<int:movie_id>/overview', methods=['GET'])(api_movie_overview)

