db = db.getSiblingDB(_getEnv("MONGODB_DB_AUTH"));

db.createUser(
  {
    user: _getEnv("MONGODB_USER"),
    pwd: _getEnv("MONGODB_PASSWORD"),
    roles: [
       { role: "readWrite", db: _getEnv("MONGODB_DB_AUTH") },
    ]
  }
)
