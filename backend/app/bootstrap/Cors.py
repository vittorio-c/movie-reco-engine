from flask_cors import CORS


class CorsManager:
    def __init__(self, app):
        self.app = app
        self.cors_origin = app.config['BACKEND_CORS_ORIGINS']

    def authorize(self):
        origins = []

        for origin in self.cors_origin:
            use_origin = origin.strip()
            origins.append(use_origin)

        CORS(self.app, origins=origins, supports_credentials=True)

        return self.app
