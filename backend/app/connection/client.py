import os

import certifi
import pymongo


def get_db_connection():
    certifi.where()

    password = os.environ["MONGODB_PASSWORD"]
    user = os.environ["MONGODB_USER"]
    host = os.environ["MONGODB_HOST"]
    db_auth = os.environ["MONGODB_DB_AUTH"]

    # Switch to srv  + tls in production :
    # client = pymongo.MongoClient(
    #     f"mongodb+srv://{user}:{password}@{host}/?authSource=movie_lens_db",
    #     tlsCAFile=ca,
    # )

    client = pymongo.MongoClient(
        f"mongodb://{user}:{password}@{host}/?authSource={db_auth}", connect=False
    )

    return client
