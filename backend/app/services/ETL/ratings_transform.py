from datetime import datetime

import petl as etl

from flask import current_app

from app.services.Kafka.consumer_factory import get_consumer
from kafka.consumer.fetcher import ConsumerRecord
from pymongo.collection import Collection


class RatingsTransformer:
    def __init__(self, payload):
        self.ratings_payload = payload
        self.ratings_out = None

    def transform_ratings(self):
        self.ratings_out = (
            etl.fromdicts(self.ratings_payload)
            .convert("userId", int)
            .convert("movieId", int)
            .convert("rating", float)
            .convert("timestamp", int)
            .convert("timestamp", lambda ts: datetime.fromtimestamp(ts))
            .rename(
                {
                    "userId": "user_id",
                    "movieId": "ml_id",
                    "timestamp": "rated_at",
                }
            )
        )

        return self

    def transform(self):
        try:
            self.transform_ratings()
            return list(etl.dicts(self.ratings_out))
        except Exception as e:
            print(e)
            import traceback

            from app.services.Kafka.producer_factory import get_producer

            error = traceback.format_exc()
            producer = get_producer("localhost:9092")
            payload = {"error": error, "ratings": self.ratings_payload}

            producer.send("ml2ml_ratings_error", payload)


topic = "ml2ml_ratings"
consumer = get_consumer(topic, "localhost:9092")
print(consumer)
message: ConsumerRecord
for message in consumer:
    print(f"Consumming message n° {message.offset} on topic {topic}\n")
    transformer = RatingsTransformer(message.value)
    ratings = transformer.transform()

    collection: Collection = current_app.db.ratings
    inserted = collection.insert_many(ratings)
