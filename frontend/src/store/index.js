import { createStore } from "vuex";
import api from "../api";

export default createStore({
  state: {
    movie_filters: {
      sorts: "release_year",
      order: "-1",
    },
    movie_urls: {},
    movies: [],
    recommended_movies: [],
    movie_pagination: {},
    currentUser: null,
    allUserIds: [],
    is_modale_open: false,
  },
  mutations: {
    set_movie_filters(state, filter) {
      state.movie_filters = filter;
    },
    set_movies(state, movies) {
      state.movies = movies;
    },
    set_movie_urls(state, urls) {
      state.movie_urls = urls;
    },
    set_movie_pagination(state, pagination) {
      state.movie_pagination = pagination;
    },
    updateCurrentPage(state, page) {
      state.movie_pagination.current_page = page;
    },
    logUser(state, userId) {
      state.currentUser = userId;
    },
    logoutUser(state) {
      state.currentUser = null;
    },
    set_recommended_movies(state, movies) {
      state.recommended_movies = movies;
    },
    set_all_user_ids(state, userIds) {
      state.allUserIds = userIds;
    },
    set_is_modale_open(state, isOpen) {
      state.is_modale_open = isOpen;
    },
  },
  actions: {
    GET_MOVIES_PAGINATED({ commit }, { filters, page_nb }) {
      return api
        .getMoviesPaginated(filters, page_nb)
        .then((response) => {
          commit("set_movies", response.data._data);
          commit("set_movie_urls", response.data._embed);
          commit("set_movie_pagination", response.data._meta);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    GET_RECOMMENDED_MOVIES({ commit }) {
      return api
        .getRecommandedMovies()
        .then((response) => {
          commit("set_recommended_movies", response.data._data);
        })
        .catch((error) => {
          console.error(error);
        });
    },

    // eslint-disable-next-line no-unused-vars
    LOGIN({ commit }, { userId }) {
      return api
        .login({ userId })
        .then((response) => {
          const data = response.data;
          // TODO ceci est très fragile, au moindre rechargement de la page il faut se reco...
          // TODO Utiliser des cookies plutôt
          commit("logUser", data.user_id);
          // Petit bug ici, il faut peut-être clean le storage avant de récrire dedans
          localStorage.jwtToken = data.access_token;
        })
        .catch((error) => {
          window.alert(
            "Désolé une erreur est survenue. Merci de recharger la page"
          );
          console.error(error);
        });
    },

    LOGOUT({ commit }) {
      return api
        .logout()
        .then(() => {
          commit("logoutUser");
          localStorage.jwtToken = null;
          window.localStorage.removeItem("jwtToken");
        })
        .catch((error) => {
          window.alert(
            "Désolé une erreur est survenue. Merci de recharger la page"
          );
          console.error(error);
        });
    },

    REFRESH_TOKEN() {
      return api.refresh();
    },

    // eslint-disable-next-line no-unused-vars
    RATE_MOVIE({ commit }, { movieId, rating }) {
      return api.rateMovie({ movieId, rating });
    },

    ALL_USER_IDS({ commit }) {
      return api.getAllUserIds().then((response) => {
        const data = response.data;
        commit(
          "set_all_user_ids",
          data._data.map((user) => {
            return user.id;
          })
        );
      });
    },
  },
  modules: {},
});
