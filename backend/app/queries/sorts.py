def get_aggregate_sorts(sorts, order):
    aggregate = {}

    if "budget" in sorts:
        aggregate = {"$sort": {"budget": int(order)}}
    elif "revenue" in sorts:
        aggregate = {"$sort": {"revenue": int(order)}}
    elif "profit" in sorts:
        aggregate = {"$sort": {"profit": int(order)}}
    elif "release_year" in sorts:
        aggregate = {"$sort": {"release_year": int(order), "ml_id": 1}}
    elif "title" in sorts:
        aggregate = {"$sort": {"title": int(order)}}
    elif "ml_mean_rating" in sorts:
        aggregate = {"$sort": {"ml_mean_rating": int(order)}}

    return aggregate
