import axios from "axios";

const base_url = `${process.env.VUE_APP_BACKEND_DOMAIN}/api`;

function getAuthenticatedConfig() {
  if (localStorage.jwtToken !== null && localStorage.jwtToken !== undefined) {
    const jwtToken = localStorage.jwtToken;

    return {
      headers: { Authorization: `Bearer ${jwtToken}` },
    };
  } else {
    return {};
  }
}

export default {
  getMoviesPaginated({ sorts, order }, page) {
    let url_params = `/movies/?page=${page}`;
    url_params += `&sorts[]=${sorts}`;
    url_params += `&order=${order}`;

    return axios.get(base_url + url_params);
  },

  getMovieShow({ movieId }) {
    let url_params = `/movies/${movieId}`;

    return axios.get(base_url + url_params);
  },

  getSimilarMovies({ movieId }) {
    let url_params = `/movies/${movieId}/similar-movies`;

    return axios.get(base_url + url_params);
  },

  getMovieOverview({ movieId }) {
    let url_params = `/movies/${movieId}/overview`;

    return axios.get(base_url + url_params);
  },

  login({ userId }) {
    let url = `${base_url}/auth/login`;

    return axios.post(url, {
      user_id: userId,
    });
  },

  logout() {
    let url = `${base_url}/auth/logout`;

    return axios.delete(url, getAuthenticatedConfig());
  },

  rateMovie({ movieId, rating }) {
    let url = `${base_url}/ratings/rate/${movieId}/${rating}`;

    return axios.post(url, {}, getAuthenticatedConfig());
  },

  getRecommandedMovies() {
    let url = `${base_url}/users/me/recommended-movies`;

    return axios.get(url, getAuthenticatedConfig());
  },

  refresh() {
    let url = `${base_url}/auth/refresh`;

    return axios.get(url, getAuthenticatedConfig());
  },

  getAllUserIds() {
    let url = `${base_url}/users`;

    return axios.get(url);
  },
};
