class User:
    def __init__(self):
        self._id: int = 0

    def to_json(self):
        return {
            "id": self.id,
        }

    # TODO created_at depuis le champs _id de la DB
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value


class UserFactory:
    def __init__(self, payload: dict):
        self.payload = payload

    def factory(self):
        user = User()
        try:
            user.id = self.payload["user_id"]
        except KeyError as e:
            print(e)
            return None

        return user

