from flask import Flask
from flask_jwt_extended import JWTManager
from flask_caching import Cache

from app.config import config
from app.bootstrap.Cors import CorsManager
from app.connection.client import get_db_connection
from app.utilities.json import set_custom_json_encoder


class AppBootstrapper:

    @staticmethod
    def boostrap():
        app = Flask('ml2ml')
        app.config.from_object(config)
        client = get_db_connection()
        app.client = client
        app.db = app.client.ml2ml

        cors = CorsManager(app=app)
        app = cors.authorize()
        jwt = JWTManager(app)
        app.jwt = jwt

        cache = Cache(app)
        app.cache = cache

        # extend json encoder
        set_custom_json_encoder()

        return app
