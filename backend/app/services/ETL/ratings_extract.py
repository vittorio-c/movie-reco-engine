from app.services.ETL.csv_extract_params import CsvExtractParams
from app.services.ETL.ratings_extractor import RatingsExtractor

params = CsvExtractParams()
params.csv_path = "/home/<user_name>/Documents/movie_lens_data_big/ratings.csv"
params.start_after = 0
# This will take all the ratings for the first 1019 movies, regarding their ml_ids :
# 51*240 = first 12019 lines of csv file
params.batch_count = 51
params.batch_size = 240
params.topic = "ml2ml_ratings"

# csv_extractor = CsvBatchExtractor(params)
ratings_extractor = RatingsExtractor(params)
# csv_extractor.extract()
ratings_extractor.extract()
