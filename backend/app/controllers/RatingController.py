from flask_jwt_extended import jwt_required
from flask_jwt_extended import current_user
from flask import jsonify

from app.queries.ratings import rate_movie as rate_movie_query


@jwt_required()
def rate_movies(movie_id, rating):
    user = current_user
    rate_movie_query(user['user_id'], movie_id, rating)
    return jsonify(), 201
