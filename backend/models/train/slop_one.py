import pandas as pd
from surprise import Reader
from surprise import Dataset
from surprise.model_selection import train_test_split

# TODO add data folder path
df_movies = pd.read_csv('<path_to_data_folder>/movies.csv')
df_rating = pd.read_csv('<path_to_data_folder>/ratings.csv')
df_rating = df_rating[df_rating['movieId'].between(1, 1000)]
df_rating = df_rating.drop('timestamp', axis=1)

reader = Reader(rating_scale=(1, 5))
data = Dataset.load_from_df(df_rating[['userId', 'movieId', 'rating']], reader)

trainset, testset = train_test_split(data, test_size=.25, random_state=42)

from surprise import SlopeOne

slop_one = SlopeOne()
slop_one.fit(trainset)

import pickle

pickle.dump(slop_one, open('models/serialized/slop_one_regular.pkl', 'wb'))

