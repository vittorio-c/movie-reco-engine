import re


def calculate_profit(row):
    if row.tmdb_budget and row.tmdb_revenue:
        return row.tmdb_revenue - row.tmdb_budget

    return None


def split_genres(row):
    return split_row(row, "genres", "|")


def split_companies(row):
    return split_row(row, "tmdb_companie_name", "|")


def split_companies_iso(row):
    return split_row(row, "tmdb_companie_iso", "|")


def split_row(row, col, deli):
    if getattr(row, col, None) is not None:
        return getattr(row, col).split(deli)

    return None


def extract_release_year(row):
    try:
        return re.search("\((\d.+?)\)", row.title).group(1)
    except AttributeError:
        return None
