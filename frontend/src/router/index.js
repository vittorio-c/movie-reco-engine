import { createRouter, createWebHistory } from "vue-router";
import Ping from "../views/Ping.vue";
import MoviesIndexTemplate from "@/components/templates/MoviesIndexTemplate";
import MoviesIndexPage from "@/components/pages/MoviesIndexPage";
import Test from "@/components/pages/Test";
import MovieShowTemplate from "@/components/templates/MovieShowTemplate";
import MovieShowPage from "@/components/pages/MovieShowPage";
import MoviesRecommendedPage from "@/components/pages/MoviesRecommendedPage";
import store from "../store";

const routes = [
  {
    path: "/",
    redirect: "/movies",
  },
  {
    path: "/ping",
    name: "Ping",
    component: Ping,
  },
  {
    path: "/movies-template",
    name: "MoviesTemplate",
    component: MoviesIndexTemplate,
  },
  {
    path: "/movies",
    name: "MoviesIndexPage",
    component: MoviesIndexPage,
  },
  {
    path: "/movie-template/:id",
    name: "MovieShowTemplate",
    component: MovieShowTemplate,
  },
  {
    path: "/movie/:id",
    name: "MovieShowPage",
    component: MovieShowPage,
  },
  {
    path: "/test",
    name: "Test",
    component: Test,
  },
  {
    path: "/recommended-movies",
    name: "RecommendedMovies",
    component: MoviesRecommendedPage,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
