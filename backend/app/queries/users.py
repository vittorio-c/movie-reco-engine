from pymongo.collection import Collection

from flask import current_app


user_collection: Collection = current_app.db.users


def get_users():
    cursor = user_collection.find()

    return [user for user in cursor]


def find_user(user_id: int):
    return user_collection.find_one({'user_id': user_id})
