import math

from pymongo.cursor import Cursor
from flask import current_app

from app.models.movie import MovieFactory
from app.queries.sorts import get_aggregate_sorts

movie_collection = current_app.db.movies


def get_movie(movie_id):
    movie: Cursor = movie_collection.find_one({"ml_id": movie_id})

    return movie


def get_movies_paginated(page_size, page_num, sorts=None, order=1):
    if sorts is None:
        sorts = {}

    skips = page_size * (page_num - 1)

    limit = {"$limit": page_size}
    skip = {"$skip": skips}

    if len(sorts) > 0:
        agg_sorts = get_aggregate_sorts(sorts, order)
    else:
        # we default sorts to release_year and desc order
        # we need to add a second sort on ml_id, because
        # release_year consists only on the year
        agg_sorts = {"$sort": {"release_year": int(order), "ml_id": 1}}

    pagination_stage = [agg_sorts, skip, limit]
    count_stage = [{"$count": "count"}]
    facet = [{"$facet": {"data": pagination_stage, "count": count_stage}}]

    cursor = movie_collection.aggregate(facet)

    results = [elem for elem in cursor][0]
    # TODO reduce payload when index page
    movies = [MovieFactory(movie).factory() for movie in results["data"]]

    total = results["count"][0]["count"]
    next_page = page_num + 1
    previous_page = page_num - 1
    total_page = math.ceil(total / page_size)

    meta = {
        "total_count": total,
        "current_page": page_num,
        "total_pages": total_page,
        "next_page": next_page if next_page <= total_page else None,
        "previous_page": previous_page if previous_page > 0 else None,
    }

    output = {"_data": movies, "_meta": meta}

    return output


def get_movies():
    cursor = movie_collection.find()
    return [movie for movie in cursor]


def get_movies_stats():
    cursor = movie_collection.aggregate(
        [
            {
                "$group": {
                    "_id": "$release_year",
                    "avg_budget": {"$avg": "$budget"},
                    "avg_profit": {"$avg": "$profit"},
                    "avg_revenue": {"$avg": "$revenues"},
                }
            }
        ]
    )


def insert_or_update_movie(movie_object):
    return movie_collection.replace_one(
        {"_id": movie_object["_id"]}, movie_object, upsert=True
    )
