from flask import Blueprint

from app.controllers.UserController import user_recommended_movies, all_users

users_bp = Blueprint('users_bp', __name__)

users_bp.route('/', methods=['GET'])(all_users)
users_bp.route('/me/recommended-movies', methods=['GET'])(user_recommended_movies)
