from flask import jsonify
from flask_jwt_extended import jwt_required
from flask_jwt_extended import current_user

from app.models.movie import MovieFactoryWithPrediction
from app.models.user import UserFactory
from app.queries.movies import get_movie
from app.queries.users import get_users
from app.services.RecoEngines.slop_one import SlopOneRecommender

slop_one = SlopOneRecommender.get_model()


# TODO return predicted note and put in cache results
@jwt_required()
def user_recommended_movies():
    service = SlopOneRecommender(current_user["user_id"], slop_one)
    recommended_movies_ids = service.recommend()
    best_predicted = recommended_movies_ids[:10]
    recommended_movies = [get_movie(int(movie_id)) for [movie_id, _] in best_predicted]

    for (key, data) in enumerate(best_predicted):
        recommended_movies[key]["prediction"] = best_predicted[key][1]

    movies = [MovieFactoryWithPrediction(movie).factory() for movie in recommended_movies]

    return jsonify({"_data": movies})


def all_users():
    users = [UserFactory(user).factory() for user in get_users()]

    return jsonify({"_data": users})

