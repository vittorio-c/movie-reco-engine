# Set up JWT loader
from app.main import app
from app.queries.users import find_user


@app.jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return find_user(identity)


jwt_blacklist = set()


@app.jwt.token_in_blocklist_loader
def check_if_token_in_blacklist(jwt_header, jwt_payload: dict):
    jti = jwt_payload["jti"]
    return jti in jwt_blacklist
