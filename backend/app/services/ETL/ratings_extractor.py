import pandas as pd

from app.services.ETL.csv_extract_params import CsvExtractParams
from app.services.Kafka.producer_factory import get_producer


class RatingsExtractor:
    def __init__(self, params: CsvExtractParams):
        self.csv_path = params.csv_path
        self.start_after = params.start_after
        self.batch_count = params.batch_count
        self.batch_size = params.batch_size
        self.topic = params.topic
        self.producer = get_producer("localhost:9092")

    def extract(self):
        df = pd.read_csv(self.csv_path)

        df_rating = df[df['movieId'].between(1, 1000)]

        print(df_rating.shape)

        n = 10000
        start = 0
        ratings_as_chunk = [df_rating[i:i + n] for i in range(start, df_rating.shape[0], n)]

        for idx, smaller_df in enumerate(ratings_as_chunk):
            message = smaller_df.to_dict('records')
            print(len(message))
            print(
                f"Publishing message n° {idx} on topic : {self.topic}\n"
            )
            self.producer.send(self.topic, message)
