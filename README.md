# About

This repository is the source code of a web application hosting two recommendation engines.

It has been coded for accompanying the writing of a thesis called [“Training and deployment of a recommendation engine”](https://bit.ly/3snGKDv).

It shows in action two recommendation engines based on different ML algorithms (KNN and Slop One).

It uses the data from MovieLens project : <https://grouplens.org/datasets/movielens/>, enriched with metadata from other sources (<themoviedb.org>, <imdb.com>).

Stack :

- Python / Flask
- MongoDB
- VueJS
- Docker
- Gitlab CI/CD
- SciKit-Learn
- Traefik
- Tailwind CSS

The webapp has been online in 2022, hosted behind a traefik reverse proxy, on OVH cloud provider. The deployment was automated thanks to Gitlab CI/CI (see [.gitlab-ci.yml](.gitlab-ci.yml)).

# MovieLens Recommendation Engine Project

> Folder structure is inspired from : https://github.com/tiangolo/flask-frontend-docker

## Steps to get started locally

### Backend

- Install environment :

> Make sure to use a python version that matchs the one specified in `backend/.python-version`
> for installating the venv ! For installing python versions, you can use tools like
> <https://github.com/pyenv/pyenv>

`cd backend`

`python3 -m venv venv`

`source venv/bin/activate`

> Setup your IDE to use that venv

`pip install -r requirements.txt`

- Fill env values in .env (see .env.example)

- Make sure you have your serialized models in folder. If not, see section bellow "Model training" :

```
> ls -al backend/models/serialized

-rw-r--r-- 1 user user 9,7M 14 mai   13:07 knn_item_item.pkl
-rw-r--r-- 1 user user 153M 14 mai   13:07 slop_one_regular.pkl
```

- Start your stack :

`docker-compose up -d`

### Server

There is already a server running from your container "backend", with uwsgi and nginx attached, and autoreload enabled (every 3 seconds).

However, if you need to mannualy lauch the server, i.e. to use a IDE debugger, you need to be in `backend/` folder, and add those variables :

`FLASK_APP=app.main:app MONGODB_HOST=localhost  MONGODB_USER=ml2ml  MONGODB_PASSWORD=ml2ml MONGODB_DB_AUTH=ml2ml py -m flask run`

You can also directly run your scripts and commands from the container, which already has all the necessary ENV variables.

### Debug

In Pycharm, copy this configuration :

```
Module name : flask
Parameters: run
Env variables: PYTHONUNBUFFERED=1;FLASK_APP=app.main:app;MONGODB_HOST=localhost;MONGODB_USER=ml2ml;MONGODB_PASSWORD=ml2ml;MONGODB_DB_AUTH=ml2ml
Working directory: <path_to_project>/backend
```


### Frontend

- Install front dependencies :

`yarn install`

- Launch front server :

`make watch`

- Access the app from : `http://localhost:8080/`

### DB

To create a user :

```
use ml2ml
db.createUser(
  {
    user: "ml2ml",
    pwd: "ml2ml",
    roles: [
       { role: "readWrite", db: "ml2ml" },
    ]
  }
)
```

Container mongo is started with a volume binding `./data:/data_to_import` for test data to import.

Log into your container, and start importing sequences :

```
mongoimport \
    --uri="mongodb://localhost:27017/ml2ml" \
    --collection=movies --drop \
    --file=/data_to_import/movies.json \
    -u ml2ml --authenticationDatabase ml2ml
```

> You will be prompted with a password, it is the one stored in `MONGODB_PASSWORD` variable !

```
mongoimport \
    --uri="mongodb://localhost:27017/ml2ml" \
    --collection=ratings --drop \
    --file=/data_to_import/ratings_heavy.json \
    -u ml2ml --authenticationDatabase ml2ml
```

There are a set of highly qualified users (more than 500 ratings) that you can import in your database to get started with :

```
FLASK_APP=app.main:app  \
    MONGODB_HOST=localhost  \
    MONGODB_USER=ml2ml  \
    MONGODB_PASSWORD=ml2ml \
    MONGODB_DB_AUTH=ml2ml py -m flask etl insert_users <patch_of_ratings_df_csv_file>
```

### Docker

To build images, run script in `./scripts/build-images`

Then, push your images to Gitlab Docker registry :

`docker login registry.gitlab.com`

> For the credentials, you need to provide a valid token, with API level. The username is the one associated with the token.

`docker push registry.gitlab.com/vittorio-c/movie-reco-engine/<name_of_image>`

> Issues with gitlab registry login : <https://gitlab.com/gitlab-org/gitlab/-/issues/207509>, <https://gitlab.com/gitlab-org/gitlab/-/issues/19211>

Make sure you do this each time you update the image ! Especially when you update python requirements, otherwise production won't have the changes.

### ETL with Kafka

There is an ETL associated to the project that loads data from MovieLens CSV files, transform it and insert it in MongoDB database. It uses Kafka to make it asynchronous.

First, launch a consumer from the terminal :

`MONGODB_HOST=mongo MONGODB_USER=ml2ml MONGODB_PASSWORD=ml2ml MONGODB_DB_AUTH=ml2ml py -m app.services.ETL.movies_extract` in a terminal

Then launch the producer that will transform and insert the data :

`MONGODB_HOST=mongo MONGODB_USER=ml2ml MONGODB_PASSWORD=ml2ml MONGODB_DB_AUTH=ml2ml py -m app.services.ETL.movies_transform` in an other terminal

> Be sure to go to the root of the project, where the `app` folder is located

### Ratings

To recalculate all the mean ratings (this will use Spark, so make sure it is functional before) :

```bash
FLASK_APP=app.main:app  \
    MONGODB_HOST=localhost  \
    MONGODB_USER=ml2ml  \
    MONGODB_PASSWORD=ml2ml \
    MONGODB_DB_AUTH=ml2ml py -m flask ratings calculate_mean
```

## Data import in production

These steps are used to perform the first import in MongoDB database in production.

Copy the data file to the server:

`scp data/ratings_heavy.json  debian@<server_ip>:/home/debian/movie-reco-engine/data_to_import`

Retrieve the json export files, then do the following directly in the prod mongo container:

`mongoimport --uri="mongodb://localhost:27017/ml2ml" --collection=movies --drop --file=movies.json -u ml2ml --authenticationDatabase ml2ml`

`mongoimport --uri="mongodb://localhost:27017/ml2ml" --collection=ratings --drop  --file=ratings.json -u ml2ml --authenticationDatabase ml2ml`

`mongoimport --uri="mongodb://localhost:27017/ml2ml" --collection=users --drop  --file=users.json -u ml2ml --authenticationDatabase ml2ml`

## Model training

> There is an additional repository that contains Exploratory Data Analysis and Machine Learning algorithms exploration : <https://github.com/vittorio-c/2022_recommendation_engines>

> The code referenced in `./backend/models/train/` comes from this external repository

The code to train KNN model is available at [./backend/models/train/knn_item_item_from_csv.py](./backend/models/train/knn_item_item_from_csv.py)

The code to train SlopeOne model is available at [./backend/models/train/slop_one.py](./backend/models/train/slop_one.py)

Make sure to train both model and to create corresponding `.pkl` file in `./backend/models/serialized/` folder before deploying the app.

Only first 1000 movies have been selected to limit the size of the models.

## Deploying the model

For the moment :

`scp backend/models/serialized/knn_item_item.pkl debian@<ipserver>:/home/debian/movie-reco-engine/backend/models/serialized/`

It is for the moment necessary to train model locally, and to push them on the server via `scp` command, because the models pickle files are too heavy to be included in this repo (see pickle files exclusion in `.gitignore`). And there is no CD yet for training and deploying the model automatically.

