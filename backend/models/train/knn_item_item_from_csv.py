import pickle

import pandas as pd

from surprise import Reader
from surprise import Dataset
from surprise import KNNBasic
from surprise.dataset import DatasetAutoFolds

# TODO add data folder path
df_rating = pd.read_csv('<path_to_data_folder>/ratings.csv').\
    sort_values(['movieId']).drop('timestamp', axis=1)

df_rating = df_rating[df_rating['movieId'].between(1, 1000)]

print(f'Shape of loaded DF is {df_rating.shape}')

reader = Reader(rating_scale=(0.5, 5))

data: DatasetAutoFolds = Dataset.load_from_df(df_rating[['userId', 'movieId', 'rating']], reader)
trainset = data.build_full_trainset()

sim_options = {'name': 'cosine',
               'user_based': False  # compute  similarities between items
               }

knn = KNNBasic(sim_options=sim_options)
knn.fit(trainset)

# Reduce size of pickled model :
del knn.trainset.ir
del knn.trainset.ur
del knn.xr
del knn.yr

pickle.dump(knn, open('models/serialized/knn_item_item.pkl', 'wb'))

print('DONE training the model')
